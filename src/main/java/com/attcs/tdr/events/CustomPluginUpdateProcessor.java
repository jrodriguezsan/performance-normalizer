/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.attcs.tdr.events.pipeline.messages.CustomPlugins;
import com.attcs.tdr.events.service.CustomPluginsLoaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Component
@Slf4j
public class CustomPluginUpdateProcessor {
    private final CustomPluginsLoaderService customPluginsLoader;
    private final LocalPluginManager pluginManager;

    public CustomPluginUpdateProcessor(CustomPluginsLoaderService customPluginsLoader, LocalPluginManager pluginManager) {
        this.customPluginsLoader = customPluginsLoader;
        this.pluginManager = pluginManager;
    }

    /**
     * created the bean which is listening to the kafka topic for custom plugin update/create/delete
     */
    @Bean
    public Consumer<Message<CustomPlugins>> customPluginProcessor() {
        log.info("Loading updated Custom plugin ...");
        return pluginsMessage -> {
            try {
                CustomPlugins customPlugins = pluginsMessage.getPayload();
                String pluginAction = customPlugins.getAction();
                switch (pluginAction) {
                    case "CREATE":
                        customPluginsLoader.loadCustomPlugin(String.valueOf(customPlugins.getId()));
                        break;
                    case "UPDATE":
                        customPluginsLoader.unLoadCustomPlugin(customPlugins.getPluginName(), customPlugins.getPluginVersion());
                        customPluginsLoader.loadCustomPlugin(String.valueOf(customPlugins.getId()));
                        break;
                    case "DELETE":
                        customPluginsLoader.unLoadCustomPlugin(customPlugins.getPluginName(), customPlugins.getPluginVersion());
                        break;
                    default:
                        log.error("Invalid Plugin action");
                        break;
                }
                if (log.isInfoEnabled()) {
                    log.info("Normalizer configured successfully with '{}' custom plugins!",
                            pluginManager.getLoadedPlugins(true).size());
                }
            } catch (Exception e) {
                log.error("Error processing plugin update for custom plugins", e);
            }
        };
    }
}