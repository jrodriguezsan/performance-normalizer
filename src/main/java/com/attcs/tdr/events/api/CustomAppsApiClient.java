/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events.api;

import com.attcs.tdr.events.pipeline.api.ApiClient;
import com.attcs.tdr.events.pipeline.api.CustomResponseErrorHandler;
import com.attcs.tdr.events.pipeline.api.UrlBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CustomAppsApiClient extends ApiClient {
    public CustomAppsApiClient(RestTemplate restTemplate,
                               @Qualifier("customAppsUriBuilder") UrlBuilder urlBuilder,
                               CustomResponseErrorHandler restTemplateCustomResponseErrorHandler) {
        super(restTemplate, urlBuilder, restTemplateCustomResponseErrorHandler);
    }
}
