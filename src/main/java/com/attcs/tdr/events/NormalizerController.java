/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */
package com.attcs.tdr.events;


import com.attcs.tdr.events.pipeline.kafka.TdrKafkaHeaders;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.List;
import java.util.concurrent.locks.LockSupport;
import java.util.function.Supplier;

@RestController
public class NormalizerController {

    private final Sinks.Many<Message<LogEvent>> processor = Sinks.many().multicast().onBackpressureBuffer();

    @Bean
    public Supplier<Flux<Message<LogEvent>>> supplierEvents() {
        return processor::asFlux;
    }


    @RequestMapping(value = "/test/{key}", method = RequestMethod.POST)
    public String testEndpoint(@PathVariable("key") String key,
                               @RequestBody PacketBody packetBody) {

        LogEvent logEvent = new LogEvent();
        logEvent.setSyslogEvent(false);
        logEvent.setRawLog(packetBody.log);
        logEvent.setPlugins(packetBody.plugins);

        Message<LogEvent> message = MessageBuilder.withPayload(logEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, key)
                .setHeader(TdrKafkaHeaders.TENANT_ID, packetBody.tenantId)
                .setHeader(TdrKafkaHeaders.TENANT_SUBDOMAIN, "test")
                .build();

        while (this.processor.tryEmitNext(message).isFailure()) {
            LockSupport.parkNanos(10);
        }

        return "Event sent UUID!";
    }

    private static class PacketBody {
        public String log;
        public String tenantId;
        public List<String> plugins;
    }
}
