/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.dsl.DSLLogger;
import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.alienvault.atlas.api.normalizer.plugins.Normalizer;
import com.alienvault.atlas.api.packets.Packet;
import com.alienvault.atlas.common.packets.event.NormalizedEvent;
import com.attcs.tdr.events.api.CustomAppsApiClient;
import com.attcs.tdr.events.metrics.NormalizerMetricsService;
import com.attcs.tdr.events.pipeline.PipelineProcessor;
import com.attcs.tdr.events.pipeline.messages.Event;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import com.attcs.tdr.events.service.CustomPluginsLoaderService;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@EnableConfigurationProperties(NormalizerConfiguration.class)
public class NormalizerProcessor implements PipelineProcessor<LogEvent, Event> {

    private final static Logger log = LoggerFactory.getLogger(NormalizerProcessor.class);
    private final Normalizer normalizer;
    private final NormalizerConfiguration normalizerConfiguration;
    private final EventPacketBuilder eventPacketBuilder;

    private final NormalizerMetricsService metricsService;
    private final CustomAppsApiClient client;
    private final CustomPluginsLoaderService customPluginsLoader;
    private final LocalPluginManager pluginManager;

    @Getter
    private boolean normalizerReady;

    public NormalizerProcessor(NormalizerConfiguration normalizerConfiguration,
                               EventPacketBuilder eventPacketBuilder,
                               NormalizerMetricsService metricsService, CustomAppsApiClient client, LocalPluginManager pluginManager, CustomPluginsLoaderService customPluginsLoader) {
        this.client = client;
        this.customPluginsLoader = customPluginsLoader;
        this.pluginManager = pluginManager;
        this.normalizer = Normalizer.getInstance();
        this.normalizerConfiguration = normalizerConfiguration;
        this.eventPacketBuilder = eventPacketBuilder;
        this.normalizerReady = false;
        this.metricsService = metricsService;
    }

    @PostConstruct
    public void postConstruct() {
        this.initialize();
    }

    public void initialize() {
        log.info("Initializing normalizer...");

        normalizer.setPluginManager(pluginManager);

        try {
            normalizer.setup(normalizerConfiguration.getTimePatternFile(),
                    normalizerConfiguration.getScriptsDir(),
                    normalizerConfiguration.getPluginsDir(),
                    false);
            customPluginsLoader.fetchAndLoadCustomPluginList();
            this.normalizerReady = true;
        } catch (Exception ex) {
            log.error("Unable to load normalizer configuration! ", ex);
            throw ex;
        }
        if (log.isInfoEnabled()) {
            log.info("Normalizer configured successfully with total '{}' plugins! and contains '{}' custom plugin",
                    pluginManager.getLoadedPlugins(true).size(), customPluginsLoader.getCustomPluginCount());
        }
    }

    @Override
    public Event process(LogEvent logEvent) {
        long startTime = System.currentTimeMillis();

        Packet packet = eventPacketBuilder.createPacket(logEvent)
                .orElseThrow(() -> new RuntimeException("The event log was null in the event."));
        long packetCreatedTime = System.currentTimeMillis();

        NormalizedEvent normalizedEvent = this.processPacket(packet);
        long endTime = System.currentTimeMillis();

        this.metricsService.updatePacketMetrics(startTime, packetCreatedTime, endTime, packet);
        return new Event(normalizedEvent.getMessageMap());
    }

    /**
     * Sends a packet for normalisation.
     *
     * @param packet The packet to normalize.
     * @return The NormalizedEvent retrieved by the normalizer.
     */
    private NormalizedEvent processPacket(Packet packet) {
        NormalizedEvent event = normalizer.processIncomingPacket(packet, DSLLogger.NOOP);
        if (log.isTraceEnabled()) {
            log.trace("Processed event: '{}'", event.getMessageMap());
        }
        return event;
    }
}
