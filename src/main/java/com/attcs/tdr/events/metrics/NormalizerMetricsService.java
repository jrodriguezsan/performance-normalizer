/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events.metrics;

import com.alienvault.atlas.api.packets.Packet;
import com.alienvault.atlas.common.packets.data.PacketForPlugin;
import com.alienvault.atlas.common.packets.data.SyslogPacket;
import com.alienvault.atlas.common.packets.data.SyslogPacketForPlugin;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Service
@Slf4j
public class NormalizerMetricsService {

    private static final String PACKET_PROCESS_TIME = "normalizer.packet_process_time";
    private static final String PACKET_CREATION_TIME = "normalizer.packet_creation_time";
    private static final String PACKET_NORMALIZATION_TIME = "normalizer.packet_normalization_time";

    private static final String PLUGIN_USED = "normalizer.plugin_used";
    private static final String PLUGIN_NAME_TAG = "normalizer.plugin_name";
    private static final String EVENTS_WITH_PLUGIN = "normalizer.event_with_plugin";
    private static final String EVENTS_WITH_NO_PLUGIN = "normalizer.event_with_no_plugin";

    private static final String PACKET_TYPE_GAUGE_TAG_NAME = "packet_type";

    private static final String SYSLOG_PACKET_TAG = "syslog_packet";
    private static final String SYSLOG_PACKET_FOR_PLUGIN_TAG = "syslog_packet_for_plugin";
    private static final String PACKET_FOR_PLUGIN_TAG = "packet_for_plugin";

    private final Map<String, AtomicLong> packetProcessGauges;
    private final Map<String, AtomicLong> packetCreationTimeGauges;
    private final Map<String, AtomicLong> packetNormalizationTimeGauges;

    private final Map<String, Counter> pluginsMap;
    private final Counter eventsWithPlugin;
    private final Counter eventsWithNoPlugin;

    private final MeterRegistry meterRegistry;

    public NormalizerMetricsService(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
        this.packetProcessGauges = new HashMap<>();
        this.packetCreationTimeGauges = new HashMap<>();
        this.packetNormalizationTimeGauges = new HashMap<>();
        this.pluginsMap = new HashMap<>();


        AtomicLong syslogGauge1 = meterRegistry.gauge(PACKET_PROCESS_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_TAG), new AtomicLong(0L));
        AtomicLong syslogGauge2 = meterRegistry.gauge(PACKET_CREATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_TAG), new AtomicLong(0L));
        AtomicLong syslogGauge3 = meterRegistry.gauge(PACKET_NORMALIZATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_TAG), new AtomicLong(0L));

        AtomicLong syslogForPluginGauge1 = meterRegistry.gauge(PACKET_PROCESS_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));
        AtomicLong syslogForPluginGauge2 = meterRegistry.gauge(PACKET_CREATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));
        AtomicLong syslogForPluginGauge3 = meterRegistry.gauge(PACKET_NORMALIZATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, SYSLOG_PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));

        AtomicLong packetForPluginGauge1 = meterRegistry.gauge(PACKET_PROCESS_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));
        AtomicLong packetForPluginGauge2 = meterRegistry.gauge(PACKET_CREATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));
        AtomicLong packetForPluginGauge3 = meterRegistry.gauge(PACKET_NORMALIZATION_TIME, Tags.of(PACKET_TYPE_GAUGE_TAG_NAME, PACKET_FOR_PLUGIN_TAG), new AtomicLong(0L));

        this.packetProcessGauges.put(SYSLOG_PACKET_TAG, syslogGauge1);
        this.packetProcessGauges.put(SYSLOG_PACKET_FOR_PLUGIN_TAG, syslogForPluginGauge1);
        this.packetProcessGauges.put(PACKET_FOR_PLUGIN_TAG, packetForPluginGauge1);

        this.packetCreationTimeGauges.put(SYSLOG_PACKET_TAG, syslogGauge2);
        this.packetCreationTimeGauges.put(SYSLOG_PACKET_FOR_PLUGIN_TAG, syslogForPluginGauge2);
        this.packetCreationTimeGauges.put(PACKET_FOR_PLUGIN_TAG, packetForPluginGauge2);

        this.packetNormalizationTimeGauges.put(SYSLOG_PACKET_TAG, syslogGauge3);
        this.packetNormalizationTimeGauges.put(SYSLOG_PACKET_FOR_PLUGIN_TAG, syslogForPluginGauge3);
        this.packetNormalizationTimeGauges.put(PACKET_FOR_PLUGIN_TAG, packetForPluginGauge3);

        this.eventsWithPlugin = meterRegistry.counter(EVENTS_WITH_PLUGIN);
        this.eventsWithNoPlugin = meterRegistry.counter(EVENTS_WITH_NO_PLUGIN);
    }

    /**
     * Updates normalization metrics:
     * * Total time needed to process a packet
     * * Total time needed to guess the packet type
     * * Total time needed to normalize a packet
     *
     * Gauges will store this info since the processing time is dynamic and can be lower than a previous value.
     * Depending on the packet type, we update the appropriate tagged gauge.
     *
     * @param startTime         Starting time where the packet was processed.
     * @param packetCreatedTime Time it took for the packet to be created.
     * @param endTime           Time where the process finished.
     * @param packet            The packet used, depending on the type we update a type of gauge.
     */
    public void updatePacketMetrics(long startTime, long packetCreatedTime, long endTime, Packet packet) {
        long processTime = endTime - startTime;
        long packetCreationTime = packetCreatedTime - startTime;
        long normalizationTime = endTime - packetCreatedTime;

        String packetType = null;

        if (packet instanceof SyslogPacketForPlugin) {
            packetType = SYSLOG_PACKET_FOR_PLUGIN_TAG;
        } else if (packet instanceof SyslogPacket) {
            packetType = SYSLOG_PACKET_TAG;
        } else if (packet instanceof PacketForPlugin) {
            packetType = PACKET_FOR_PLUGIN_TAG;
        }

        if (packetType != null) {
            this.packetProcessGauges.get(packetType).set(processTime);
            this.packetCreationTimeGauges.get(packetType).set(packetCreationTime);
            this.packetNormalizationTimeGauges.get(packetType).set(normalizationTime);
        } else {
            log.debug("Unrecognized packet arrived for metrics.");
        }
    }

    public void incrementEventsWithPlugins() {
        this.eventsWithPlugin.increment();
    }

    public void incrementEventsWithNoPlugins() {
        this.eventsWithNoPlugin.increment();
    }

    public void incrementPluginUsed(String pluginName) {
        if (!this.pluginsMap.containsKey(pluginName)) {
            Counter pluginCounter = this.meterRegistry.counter(PLUGIN_USED, Tags.of(PLUGIN_NAME_TAG, pluginName));
            pluginCounter.increment();
            this.pluginsMap.put(pluginName, pluginCounter);
        } else {
            this.pluginsMap.get(pluginName).increment();
        }
    }

}
