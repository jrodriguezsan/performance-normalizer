/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.attcs.tdr.events.pipeline.PipelineProcessor;
import com.attcs.tdr.events.pipeline.PipelineService;
import com.attcs.tdr.events.pipeline.messages.Event;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import com.attcs.tdr.events.pipeline.metrics.PipelineMetrics;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;

import java.util.function.Function;

@Slf4j
public class NormalizerService extends PipelineService<LogEvent, Event> {

    protected NormalizerService(PipelineProcessor<LogEvent, Event> processor,
                                PipelineMetrics pipelineMetrics) {
        super(processor, pipelineMetrics);
    }

    /**
     * Bean to process incoming messages and send them to the output sink
     */
    @Bean
    public Function<Message<LogEvent>, Message<Event>> pipelineProcess() {
        return input -> {
            try {
                return this.internalProcess(input);
            } catch (Exception ex) {
                log.error("Cannot normalize event. Sending it to DLQ", ex);
                throw ex;
            }
        };
    }
}
