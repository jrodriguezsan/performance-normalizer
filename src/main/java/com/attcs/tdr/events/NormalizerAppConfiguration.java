/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.attcs.tdr.events.pipeline.PipelineServiceConfiguration;
import com.attcs.tdr.events.pipeline.api.UrlBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
@Import(NormalizerService.class)
public class NormalizerAppConfiguration extends PipelineServiceConfiguration {

    @Bean
    public LocalPluginManager getPluginManager() {
       return new LocalPluginManager();
    }

    @Bean(name = "customAppsUriBuilder")
    public UrlBuilder getCustomAppsUriBuilder(@Value("${custom-apps-url}") String customAppsUrl) {
        return new UrlBuilder(customAppsUrl);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        // This bean should not be necessary, but it will help with the testing
        return restTemplateBuilder.setConnectTimeout(Duration.ofMinutes(1))
                .setReadTimeout(Duration.ofMinutes(1))
                .build();
    }
}
