/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events.service;

import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.alienvault.atlas.api.normalizer.plugins.Plugin;
import com.alienvault.atlas.api.normalizer.plugins.PluginParser;
import com.alienvault.atlas.common.apps.impl.assets.PluginImpl;
import com.attcs.tdr.events.CustomPluginResponse;
import com.attcs.tdr.events.api.CustomAppsApiClient;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class CustomPluginsLoaderService {
    private final LocalPluginManager localPluginManager;
    private static final String BASE_URL = "plugin";
    private final CustomAppsApiClient customAppsApiClient;
    private int loadedPluginCount;

    @Autowired
    public CustomPluginsLoaderService(LocalPluginManager localPluginManager, CustomAppsApiClient customAppsApiClient) {
        this.localPluginManager = localPluginManager;
        this.customAppsApiClient = customAppsApiClient;
    }

    /**
     * fetch the paginated list of plugins and load the custom plugins
     */
    public void fetchAndLoadCustomPluginList() {
        int pageSize = 5;
        int currentPage = 0;
        int loadedPluginCount;
        do {
            loadedPluginCount = loadCustomPlugin(pageSize, currentPage);
            currentPage++;
        } while (pageSize == loadedPluginCount);
    }

    /**
     * fetch the custom plugin list and register the plugin
     *
     * @param pageSize total number of data per page
     * @param page     current page
     * @return total loaded plugin
     */
    private Integer loadCustomPlugin(int pageSize, int page) {
        loadedPluginCount = 0;
        List<CustomPluginResponse> customPlugins = getPluginList(pageSize, page);
        for (CustomPluginResponse response : customPlugins) {
            try {
                Reader reader = new StringReader(response.getPlugin());
                Plugin plugin = PluginParser.readJsonPlugin(reader);
                localPluginManager.registerPlugin(plugin);
                loadedPluginCount++;
            } catch (Exception ex) {
                log.error("Unable to read the custom plugin info ", ex);
            }
        }
        return customPlugins.size();
    }

    /**
     * fetch the custom plugin and register the plugin
     *
     * @param id --> customPluginId
     */
    public void loadCustomPlugin(String id) {
        CustomPluginResponse response = getPluginById(id);
        try {
            Reader reader = new StringReader(response.getPlugin());
            Plugin plugin = PluginParser.readJsonPlugin(reader);
            localPluginManager.registerPlugin(plugin);
        } catch (Exception ex) {
            log.error("Unable to read the custom plugin info ", ex);
        }
    }

    /**
     * it will remove the plugin from loaded plugin list
     *
     * @param pluginName    name of the custom plugin
     * @param pluginVersion version of custom plugin
     */
    public void unLoadCustomPlugin(String pluginName, String pluginVersion) {
        PluginImpl plugin = new PluginImpl();
        plugin.setName(pluginName);
        plugin.setVersion(pluginVersion);
        localPluginManager.unloadPlugin(plugin);
    }

    /**
     * @return total number of custom plugin created
     */
    public int getCustomPluginCount() {
        return loadedPluginCount;
    }

    /**
     * This return list of custom plugin which has been created by custom apps
     *
     * @param pageSize total number of data per page
     * @param page     current page
     * @return list of custom plugin
     */
    protected List<CustomPluginResponse> getPluginList(int pageSize, int page) {
        try {
            String customPlugin = executeRequest(pageSize, page);
            return parseResponseForPluginList(customPlugin);
        } catch (Exception ex) {
            log.warn("Unable to parse the custom plugin", ex);
        }
        return Collections.emptyList();
    }

    /**
     * This return custom plugin by given id which has been created by custom apps
     *
     * @param id --> customPluginId
     * @return custom plugin
     */
    protected CustomPluginResponse getPluginById(String id) {
        try {
            String customPlugin = executeRequest(id);
            return parseResponse(customPlugin);
        } catch (Exception ex) {
            log.warn("Unable to parse the custom plugin", ex);
        }
        return new CustomPluginResponse();
    }

    private List<CustomPluginResponse> parseResponseForPluginList(String object) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        if (object == null) {
            log.error("Null response while pulling custom plugin");
            return Collections.emptyList();
        }
        return objectMapper.reader()
                .forType(new TypeReference<List<CustomPluginResponse>>() {
                })
                .readValue(object);
    }

    private CustomPluginResponse parseResponse(String object) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        if (object == null) {
            log.error("Null response while pulling custom plugin");
            return new CustomPluginResponse();
        }
        return objectMapper.reader()
                .forType(new TypeReference<CustomPluginResponse>() {
                })
                .readValue(object);
    }

    private String executeRequest(int pageSize, int page) {
        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("size", pageSize);
        requestParams.put("page", page);
        try {
            return customAppsApiClient.get(BASE_URL, requestParams, String.class);
        } catch (RestClientException exception) {
            log.error("Custom App is not up and running", exception);
        }
        return null;
    }

    private String executeRequest(String id) {
        try {
            return customAppsApiClient.get(BASE_URL + "/" + id, String.class);
        } catch (RestClientException exception) {
            log.error("Custom App is not up and running", exception);
        }
        return null;
    }
}
