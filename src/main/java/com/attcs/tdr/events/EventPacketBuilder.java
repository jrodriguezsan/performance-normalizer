/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.packets.Packet;
import com.alienvault.atlas.common.packets.data.PacketForPlugin;
import com.alienvault.atlas.common.packets.data.SyslogPacket;
import com.alienvault.atlas.common.packets.data.SyslogPacketForPlugin;
import com.alienvault.atlas.common.packets.event.NormalizedEvent;
import com.attcs.tdr.events.metrics.NormalizerMetricsService;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Optional;

@Component
public class EventPacketBuilder {

    private final NormalizerMetricsService metricsService;

    public EventPacketBuilder(NormalizerMetricsService metricsService) {
        this.metricsService = metricsService;
    }

    /**
     * Creates a packet based on the info stored in LogEvent. It can be:
     * SyslogPacket, SyslogPacketForPlugin, PacketForPlugin or LogPacket.
     *
     * @param event The event we are translating into a Packet.
     * @return An Optional of the packet or empty if the event was null.
     */
    public Optional<Packet> createPacket(LogEvent event) {

        if (event == null) {
            return Optional.empty();
        }

        Packet packet;
        NormalizedEvent packetNormalizedEvent = new NormalizedEvent(event.getEventMap());

        boolean hasPlugins = !CollectionUtils.isEmpty(event.getPlugins());

        if (event.isSyslogEvent()) {
            if (hasPlugins) {
                packet = SyslogPacketForPlugin.createFromPluginNameList(
                        event.getRawLog(),
                        event.getSender(),
                        event.getReceiver(),
                        packetNormalizedEvent,
                        true,
                        event.getPlugins()
                );
            } else {
                packet = new SyslogPacket(
                        event.getRawLog(),
                        event.getSender(),
                        event.getReceiver(),
                        packetNormalizedEvent,
                        true
                );
            }
        } else {
            packet = new PacketForPlugin(event.getRawLog(), event.getPlugins(), packetNormalizedEvent);
        }

        if (hasPlugins) {
            for (String plugin : event.getPlugins()) {
                this.metricsService.incrementPluginUsed(plugin);
            }
            this.metricsService.incrementEventsWithPlugins();
        } else {
            this.metricsService.incrementEventsWithNoPlugins();
        }

        return Optional.of(packet);
    }
}
