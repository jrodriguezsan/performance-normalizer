/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.attcs.tdr.events.api.CustomAppsApiClient;
import com.attcs.tdr.events.pipeline.messages.CustomPlugins;
import com.attcs.tdr.events.service.CustomPluginsLoaderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.support.MessageBuilder;

import java.util.UUID;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CustomPluginUpdateProcessorTest {
    @Mock
    CustomAppsApiClient client;
    @Mock
    CustomPluginsLoaderService customPluginsLoader;
    @Mock
    LocalPluginManager pluginManager;
    CustomPluginUpdateProcessor processor;

    @BeforeEach
    public void setUp() {
        processor = new CustomPluginUpdateProcessor(customPluginsLoader, pluginManager);

    }

    @Test
    public void pluginCreated() {
        UUID pluginId = UUID.randomUUID();
        CustomPlugins customPlugins = new CustomPlugins();
        customPlugins.setId(pluginId);
        customPlugins.setAction("CREATE");
        processor.customPluginProcessor().accept(MessageBuilder.withPayload(customPlugins).build());
        verify(customPluginsLoader).loadCustomPlugin(String.valueOf(pluginId));
    }

    @Test
    public void pluginUpdated() {
        UUID pluginId = UUID.randomUUID();
        CustomPlugins customPlugins = new CustomPlugins();
        customPlugins.setId(pluginId);
        customPlugins.setAction("UPDATE");
        customPlugins.setPluginName("test");
        customPlugins.setPluginVersion("0.2");
        processor.customPluginProcessor().accept(MessageBuilder.withPayload(customPlugins).build());
        verify(customPluginsLoader).unLoadCustomPlugin(customPlugins.getPluginName(), customPlugins.getPluginVersion());
        verify(customPluginsLoader).loadCustomPlugin(String.valueOf(pluginId));
    }

    @Test
    public void pluginDeleted() {
        CustomPlugins customPlugins = new CustomPlugins();
        customPlugins.setAction("DELETE");
        customPlugins.setPluginName("test");
        customPlugins.setPluginVersion("0.2");
        processor.customPluginProcessor().accept(MessageBuilder.withPayload(customPlugins).build());
        verify(customPluginsLoader).unLoadCustomPlugin(customPlugins.getPluginName(), customPlugins.getPluginVersion());
    }
}
