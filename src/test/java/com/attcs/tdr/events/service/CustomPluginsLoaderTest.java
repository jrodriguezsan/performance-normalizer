/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events.service;

import com.alienvault.atlas.api.normalizer.plugins.LocalPluginManager;
import com.amazonaws.util.IOUtils;
import com.attcs.tdr.events.CustomPluginResponse;
import com.attcs.tdr.events.api.CustomAppsApiClient;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class CustomPluginsLoaderTest {
    @Mock
    private LocalPluginManager localPluginManager;
    @Mock
    private CustomAppsApiClient customAppsApiClient;
    private CustomPluginsLoaderService pluginsLoader;

    @BeforeEach
    public void setUp() {
        pluginsLoader = new CustomPluginsLoaderService(localPluginManager, customAppsApiClient);
    }

    @Test
    public void testGetPlugin() {
        String uuid = "0279ed77-bb84-46ba-a3ce-1e23c98b8061";
        when(customAppsApiClient.get("plugin/"+uuid, String.class)).thenReturn(getResponseFromFile("custom-plugin.json"));
        CustomPluginResponse plugin = pluginsLoader.getPluginById(uuid);
        assertEquals(plugin.getPlugin(), "{\"name\":\"TestPlugin1\",\"device\":\"TestPlugin1\",\"type\":\"JSON\",\"appFormat\":\"JSON\",\"vendor\":\"null\",\"deviceType\":\"undefined\",\"version\":\"0.1\",\"highlight_fields\":\"legacy_src_port\",\"hints\":[],\"tags\":{\"legacy_src_port\":{\"matches\":[\"map('$.USER_ID')\",\"map('$.CLIENT_IP')\"]},\"dns_type\":{\"matches\":[\"map('$.CIPHER_SUITE')\"]}}}");
    }

    @Test
    public void testGetPluginWithNullObject() {
        String uuid = "0279ed77-bb84-46ba-a3ce-1e23c98b8061";
        when(customAppsApiClient.get("plugin/"+uuid, String.class)).thenReturn(null);
        CustomPluginResponse plugin = pluginsLoader.getPluginById(uuid);
        assertNull(plugin.getPlugin());
    }

    @Test
    public void testGetPluginWithInvalidId() {
        String uuid = "0279ed77";
        when(customAppsApiClient.get("plugin/" + uuid, String.class)).thenReturn(" [{\n" +
                "    \"id\": \"0279ed77-bb84-46ba-a3ce-1e23c98b8061\",\n" +
                "    \"tenantId\": \"a9024e2b-23d2-4e9c-ba20-cfe794b8cf30\",\n" +
                "    \"pluginName\": \"TestPlugin1\",\n" +
                "    \"pluginVersion\": \"0.1\",\n" +
                "    \"pluginType\": \"JSON\",\n" +
                "    \"pluginDevice\": \"TestPlugin1\",\n" +
                "    \"pluginVendor\": \"null\",\n" +
                "    \"plugin\": \"{\\\"name\\\":\\\"TestPlugin1\\\",\\\"device\\\":\\\"TestPlugin1\\\",\\\"type\\\":\\\"JSON\\\",\\\"appFormat\\\":\\\"JSON\\\",\\\"vendor\\\":\\\"null\\\",\\\"deviceType\\\":\\\"undefined\\\",\\\"version\\\":\\\"0.1\\\",\\\"highlight_fields\\\":\\\"legacy_src_port\\\",\\\"hints\\\":[],\\\"tags\\\":{\\\"legacy_src_port\\\":{\\\"matches\\\":[\\\"map('$.USER_ID')\\\",\\\"map('$.CLIENT_IP')\\\"]},\\\"dns_type\\\":{\\\"matches\\\":[\\\"map('$.CIPHER_SUITE')\\\"]}}}\",\n" +
                "    \"pluginStatus\": \"Pending\",\n" +
                "    \"createdBy\": \"ashu\",\n" +
                "    \"lastModifiedBy\": \"ashu\",\n" +
                "    \"created\": 1645454416691,\n" +
                "    \"lastModified\": 1645454416691\n" +
                "  }]");
        CustomPluginResponse plugin = pluginsLoader.getPluginById(uuid);
        assertNull(plugin.getPlugin());
    }

    @Test
    public void testGetPluginList() {
        when(customAppsApiClient.get(eq("plugin"),any(), eq(String.class))).thenReturn(getResponseFromFile("custom-pluginList.json"));
        List<CustomPluginResponse> list = pluginsLoader.getPluginList(2, 0);
        assertEquals(list.size(), 2);
    }

    @Test
    public void testGetPluginListWithNullObject() {
        when(customAppsApiClient.get(eq("plugin"),any(), eq(String.class))).thenReturn(null);
        List<CustomPluginResponse> list = pluginsLoader.getPluginList(0, 0);
        assertEquals(list.size(), 0);
    }

    @Test
    public void testGetPluginListWithInvalidObject() {
        when(customAppsApiClient.get("plugin" , String.class)).thenReturn(" {\n" +
                "    \"id\": \"0279ed77-bb84-46ba-a3ce-1e23c98b8061\",\n" +
                "    \"tenantId\": \"a9024e2b-23d2-4e9c-ba20-cfe794b8cf30\",\n" +
                "    \"pluginName\": \"TestPlugin1\",\n" +
                "    \"pluginVersion\": \"0.1\",\n" +
                "    \"pluginType\": \"JSON\",\n" +
                "    \"pluginDevice\": \"TestPlugin1\",\n" +
                "    \"pluginVendor\": \"null\",\n" +
                "    \"plugin\": \"{\\\"name\\\":\\\"TestPlugin1\\\",\\\"device\\\":\\\"TestPlugin1\\\",\\\"type\\\":\\\"JSON\\\",\\\"appFormat\\\":\\\"JSON\\\",\\\"vendor\\\":\\\"null\\\",\\\"deviceType\\\":\\\"undefined\\\",\\\"version\\\":\\\"0.1\\\",\\\"highlight_fields\\\":\\\"legacy_src_port\\\",\\\"hints\\\":[],\\\"tags\\\":{\\\"legacy_src_port\\\":{\\\"matches\\\":[\\\"map('$.USER_ID')\\\",\\\"map('$.CLIENT_IP')\\\"]},\\\"dns_type\\\":{\\\"matches\\\":[\\\"map('$.CIPHER_SUITE')\\\"]}}}\",\n" +
                "    \"pluginStatus\": \"Pending\",\n" +
                "    \"createdBy\": \"ashu\",\n" +
                "    \"lastModifiedBy\": \"ashu\",\n" +
                "    \"created\": 1645454416691,\n" +
                "    \"lastModified\": 1645454416691\n" +
                "  }");
        List<CustomPluginResponse> list = pluginsLoader.getPluginList(1, 0);
        assertEquals(list.size(), 0);
    }

    public String getResponseFromFile(String file) {
        try {
            return IOUtils.toString(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return StringUtils.EMPTY;
    }
}
