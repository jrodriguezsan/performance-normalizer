/*
 * AT&T Proprietary (Internal Use Only)
 * All rights reserved
 * This code is protected by copyright and distributed under licenses
 * restricting its use, copying, distribution, and decompilation. It may
 * only be reproduced or used under license from AT&T or its
 * authorised licensees.
 */

package com.attcs.tdr.events;

import com.alienvault.atlas.api.packets.keys.DefaultKey;
import com.attcs.tdr.events.metrics.NormalizerMetricsService;
import com.attcs.tdr.events.pipeline.messages.Event;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.annotation.DirtiesContext.ClassMode;

@RunWith(SpringRunner.class)
@SpringBootTest
@EmbeddedKafka(partitions = 1, topics = {
        NormalizerTest.TOPIC_EVENTS_IN,
        NormalizerTest.TOPIC_EVENTS_OUT
})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class NormalizerTest {
    public static final String TOPIC_EVENTS_IN = "topicIn";
    public static final String TOPIC_EVENTS_OUT = "topicOut";

    private final String testEvent = "<100>Jan 10 10:10:10 sshd[1097]: Failed password for invalid user pikachu from 1.1.1.1 port 43312 ssh2";
    private final String testEventWithHints = "%LINK-3-UPDOWN: Interface FastEthernet1/0/23, changed state to down";
    private final String testPlugin1 = "Linux SSH";
    private final String testPlugin2 = "Linux SUDO";

    @Autowired
    private EmbeddedKafkaBroker embeddedKafka;

    @MockBean
    private NormalizerMetricsService metricsService;

    private KafkaTemplate<String, Event> eventTemplate;
    private Consumer<String, String> consumer;

    @Before
    public void setUp() {

        // Producer
        Map<String, Object> senderProps = KafkaTestUtils.producerProps(embeddedKafka);
        senderProps.put(
                ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        senderProps.put(
                ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        DefaultKafkaProducerFactory<String, Event> pf = new DefaultKafkaProducerFactory<>(
                senderProps);
        eventTemplate = new KafkaTemplate<>(pf, true);
        eventTemplate.setDefaultTopic(TOPIC_EVENTS_IN);

        // Consumer
        Map<String, Object> consumerProps = KafkaTestUtils.consumerProps("group", "false",
                embeddedKafka);
        consumerProps.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        senderProps.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        DefaultKafkaConsumerFactory<String, String> cf = new DefaultKafkaConsumerFactory<>(consumerProps);
        consumer = cf.createConsumer();
        embeddedKafka.consumeFromAnEmbeddedTopic(consumer, TOPIC_EVENTS_OUT);
    }

    @Test
    public void testReceivePacketForPlugin() throws Exception {

        // Packet
        ObjectMapper mapper = new ObjectMapper();
        LogEvent logEvent = new LogEvent();
        logEvent.setPlugin(testPlugin1);
        logEvent.setPlugin(testPlugin2);
        logEvent.setRawLog(testEvent);

        Message<LogEvent> messageIn = MessageBuilder.withPayload(logEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();

        // Act
        eventTemplate.send(messageIn);

        ConsumerRecord<String, String> received = KafkaTestUtils.getSingleRecord(consumer, TOPIC_EVENTS_OUT, 5000);
        Event message = mapper.readValue(received.value(), Event.class);

        // Assert
        assertEquals("pikachu", message.getString(DefaultKey.SOURCE_USERNAME));
        assertEquals(testPlugin1, message.getString(DefaultKey.PLUGIN));
        assertFalse(Boolean.parseBoolean(message.getString(DefaultKey.WAS_FUZZIED)));
        assertEquals("Failed password", message.getString(DefaultKey.EVENT_NAME));
    }

    @Test
    public void testReceivePacketWithoutPlugin() throws Exception {

        // Packet
        ObjectMapper mapper = new ObjectMapper();
        LogEvent logEvent = new LogEvent();
        logEvent.setRawLog(testEventWithHints);
        logEvent.setKey(DefaultKey.CONNECTOR_ID, "connectorId");

        Message<LogEvent> messageIn = MessageBuilder.withPayload(logEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();

        // Act
        eventTemplate.send(messageIn);

        ConsumerRecord<String, String> received = KafkaTestUtils.getSingleRecord(consumer, TOPIC_EVENTS_OUT, 5000);
        Event message = mapper.readValue(received.value(), Event.class);

        // Assert
        assertEquals("Cisco Router", message.getString(DefaultKey.PLUGIN));
        assertFalse(Boolean.parseBoolean(message.getString(DefaultKey.WAS_FUZZIED)));
        assertEquals("connectorId", message.getString(DefaultKey.CONNECTOR_ID));

        // ToDo: Check why the Normalizer is not setting the USED_HINT key
        //assertTrue(Boolean.parseBoolean(message.getString(DefaultKey.USED_HINT)));
    }

    @Test
    public void testReceiveSyslogPacket() throws Exception {

        // Arrange
        ObjectMapper mapper = new ObjectMapper();
        LogEvent logEvent = new LogEvent();
        logEvent.setRawLog(testEvent);
        logEvent.setSyslogEvent(true);
        logEvent.setReceiver("130.130.130.130");
        logEvent.setSender("210.210.210.210");

        Message<LogEvent> messageIn = MessageBuilder.withPayload(logEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();

        // Act
        eventTemplate.send(messageIn);

        ConsumerRecord<String, String> received = KafkaTestUtils.getSingleRecord(consumer, TOPIC_EVENTS_OUT, 5000);
        Event message = mapper.readValue(received.value(), Event.class);

        // Assert
        assertEquals("pikachu", message.getString(DefaultKey.SOURCE_USERNAME));
        assertEquals(testPlugin1, message.getString(DefaultKey.PLUGIN));
        assertFalse(Boolean.parseBoolean(message.getString(DefaultKey.WAS_FUZZIED)));
        assertTrue(Boolean.parseBoolean(message.getString(DefaultKey.USED_HINT)));
        assertEquals("Failed password", message.getString(DefaultKey.EVENT_NAME));
    }

    @Test
    public void testReceiveSyslogPacketForPlugin() throws Exception {

        // Arrange
        ObjectMapper mapper = new ObjectMapper();
        LogEvent logEvent = new LogEvent();
        logEvent.setRawLog(testEvent);
        logEvent.setPlugin(testPlugin1);
        logEvent.setPlugin(testPlugin2);
        logEvent.setSyslogEvent(true);
        logEvent.setReceiver("130.130.130.130");
        logEvent.setSender("210.210.210.210");

        Message<LogEvent> messageIn = MessageBuilder.withPayload(logEvent)
                .setHeader(KafkaHeaders.MESSAGE_KEY, UUID.randomUUID().toString())
                .build();

        // Act
        eventTemplate.send(messageIn);

        ConsumerRecord<String, String> received = KafkaTestUtils.getSingleRecord(consumer, TOPIC_EVENTS_OUT, 5000);
        Event message = mapper.readValue(received.value(), Event.class);
        // Assert
        assertEquals("pikachu", message.getString(DefaultKey.SOURCE_USERNAME));
        assertEquals(testPlugin1, message.getString(DefaultKey.PLUGIN));
        assertFalse(Boolean.parseBoolean(message.getString(DefaultKey.WAS_FUZZIED)));
        assertFalse(Boolean.parseBoolean(message.getString(DefaultKey.USED_HINT)));
        assertEquals("Failed password", message.getString(DefaultKey.EVENT_NAME));
    }
}
