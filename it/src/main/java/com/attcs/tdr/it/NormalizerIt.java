package com.attcs.tdr.it;


import com.alienvault.atlas.api.packets.keys.DefaultKey;
import com.alienvault.atlas.api.packets.keys.Key;
import com.attcs.tdr.events.pipeline.messages.Event;
import com.attcs.tdr.events.pipeline.messages.LogEvent;
import com.attcs.tdr.events.pipeline.tests.TestEventProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;


@SpringBootApplication
@RestController
public class NormalizerIt extends TestEventProcessor<LogEvent, Event> {

    private static final String TEST_PLUGIN = "ToHandlersSSH";
    private static final String TEST_DESTINATION_IP = "1.1.1.1";
    private static final String TEST_SOURCE_USERNAME = "toHans";
    private static final String TEST_EVENT =
            String.format("<100>Jan 10 10:10:10 2.2.2.2 toHandlers[1097]: Failed password for invalid user %s from %s port 43312 ssh2",
                    TEST_SOURCE_USERNAME, TEST_DESTINATION_IP);

    public static void main(String[] args) {
        SpringApplication.run(NormalizerIt.class, args);
    }

    @Bean
    public Consumer<Message<Event>> processor() {
        return this::internalProcessor;
    }

    @RequestMapping("/test/start")
    public String testEndpoint() {
        LogEvent logEvent = new LogEvent();
        logEvent.setSyslogEvent(false);
        logEvent.setRawLog(TEST_EVENT);
        logEvent.setPlugin(TEST_PLUGIN);
        logEvent.setKey(DefaultKey.CONNECTOR_ID, "connectorId");
        this.send(logEvent);
        return "Event sent!";
    }

    @RequestMapping("/test/verify")
    public String verify() {
        if (receivedEvent == null) {
            return "FAILURE - test event not processed";
        }

        Optional<String> checkKeysResult = this.checkKeys(receivedEvent,
                List.of(
                        DefaultKey.WAS_FUZZIED,
                        DefaultKey.DESTINATION_ADDRESS,
                        DefaultKey.SOURCE_USERNAME,
                        DefaultKey.PLUGIN,
                        DefaultKey.CONNECTOR_ID
                ))
                .map(message -> "FAILURE - " + message);

        if (checkKeysResult.isPresent()) {
            return checkKeysResult.get();
        }
        if (!Boolean.parseBoolean(receivedEvent.getString(DefaultKey.WAS_FUZZIED))) {
            return "FAILURE - Invalid 'was fuzzed' value";
        }
        if (!TEST_DESTINATION_IP.equals(receivedEvent.getString(DefaultKey.DESTINATION_ADDRESS))) {
            return "FAILURE - Invalid 'destination address' value";
        }
        if (!TEST_SOURCE_USERNAME.equals(receivedEvent.getString(DefaultKey.SOURCE_USERNAME))) {
            return "FAILURE - Invalid 'source username' value";
        }
        if (!TEST_PLUGIN.equals(receivedEvent.getString(DefaultKey.PLUGIN))) {
            return "FAILURE - Invalid 'plugin' value";
        }

        return "SUCCESS";
    }

    private Optional<String> checkKeys(Event event, Collection<Key> keys) {
        return keys.stream()
                .filter(k -> !event.hasKey(k))
                .findFirst()
                .map(k -> "Key '" + k.getName() + "' not enriched");
    }
}
