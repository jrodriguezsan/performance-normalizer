FROM nexus.aveng.me:5000/alpine-base-hardened:latest

RUN apk add openjdk11-jre libstdc++ gcompat
RUN mkdir -p /tmpspring
RUN chown user:user /tmpspring
USER user:user

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} normalizer.jar
COPY target/plugins plugins/
COPY target/classes/time.formats time.formats
ENTRYPOINT exec java $JAVA_OPTIONS -jar /normalizer.jar
